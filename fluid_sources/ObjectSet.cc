/* Desc: Object Set class
 * Author: Andrei Haidu
 * Date: 11 Jul. 2013
 */

#include "ObjectSet.hh"

using namespace fluidix;

//////////////////////////////////////////////////
ObjectSet::ObjectSet()
{
}

//////////////////////////////////////////////////
ObjectSet::~ObjectSet()
{

}

//////////////////////////////////////////////////
void ObjectSet::Load()
{

}

//////////////////////////////////////////////////
void ObjectSet::Fini()
{

}

//////////////////////////////////////////////////
void ObjectSet::Init()
{

}

//////////////////////////////////////////////////
void ObjectSet::Reset()
{

}

//////////////////////////////////////////////////
void ObjectSet::Update()
{

}

//////////////////////////////////////////////////
void ObjectSet::SetParticleSetId(int _id)
{
	this->particleSetId = _id;
}

//////////////////////////////////////////////////
int ObjectSet::GetParticleSetId()
{
	return this->particleSetId;
}

//////////////////////////////////////////////////
int ObjectSet::GetLinkId()
{
    return this->linkSetId;
}

//////////////////////////////////////////////////
void ObjectSet::SetLinkSetId(int _linkSetId)
{
    this->linkSetId = _linkSetId;
}

//////////////////////////////////////////////////
void ObjectSet::SetParticleAndLinkSetId(int _particleSetId, int _linkSetId)
{
    this->particleSetId = _particleSetId;

    this->linkSetId = _linkSetId;
}

//////////////////////////////////////////////////
void ObjectSet::SetRestitutionCoef(float _coef)
{
    this->restitutionCoef = _coef;
}

//////////////////////////////////////////////////
float ObjectSet::GetRestitutionCoef()
{
    return this->restitutionCoef;
}

//////////////////////////////////////////////////
void ObjectSet::SetWorldPosition(float _x, float _y, float _z)
{
    this->posX = _x;
    this->posY = _y;
    this->posZ = _z;
}

//////////////////////////////////////////////////
void ObjectSet::GetWorldPosition(float &_pos_x, float &_pos_y, float &_pos_z)
{
    _pos_x = this->posX;
    _pos_y = this->posY;
    _pos_z = this->posZ;
}

//////////////////////////////////////////////////
void ObjectSet::SetWorldOrientation(float _quat_w,
			float _quat_x, float _quat_y, float _quat_z)
{
    this->quatW = _quat_w;
    this->quatX = _quat_x;
    this->quatY = _quat_y;
    this->quatZ = _quat_z;
}

//////////////////////////////////////////////////
void ObjectSet::GetWorldOrientation(float &_quat_w,
		float &_quat_x, float &_quat_y, float &_quat_z)
{
    _quat_w = this->quatW;
    _quat_x = this->quatX;
    _quat_y = this->quatY;
    _quat_z = this->quatZ;
}

