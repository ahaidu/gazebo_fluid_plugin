#include "FluidEngine.hh"
#include "../fluidix/fluidix.h"
#include "math_functions.h"

#include "quaternion.h"

//#define DEBUG
#define GRAVITY 9.80665f 		// gravity (m/s2)
#define DT 0.005f 				// time-step (s)
#define DT_MS DT*1000			// time-step (ms)
#define REAL_TIME_FACTOR 1	// real time factor
#define MAX_NR_SETS 10 		// maximum nr of sets, global arrays are created with this size

using namespace fluidix;

//////////////////////////////////////////////////
// Global variables
// Needed for external communication with Fluidix

struct ParticleSetConstants
{
	float particle_mass;		// particle mass (kg)
	float stiffness;			// pressure stiffness constant (k)
	float density;				// rest density (kg/m3)
	float viscosity;			// viscosity (Pa.s)
	float buoyancy;				// gas buoyancy  (m/s2)
	float threshold;    		// surface tension threshold (4.32)
	float tension;				// surface tension (N/m)
	float smth_neigh_part_nr;	// avg number of neighbors (for smoothing length)
	float h;					// smoothing length (m)
	float restitution; 			// object restitution coef (1 - elastic collision , 0 - inelastic)
};

struct Particle
{
    xyz r, v, v_hstep, f; 	// position, velocity, velocity half step, force (acceleration)
    xyz normal; 			// surface normal
    float density; 			// particle mass-density
    float tension; 			// surface tension force magnitude
};

struct Global
{
	xyz world_pos, world_size; 	// size and position of the world boundaries
	float wall_elasticity; 		// hard wall boundary elasticity
	xyz spawn_pos, spawn_size; 	// spawning particles position, and volume size
	xyz mesh_min, mesh_max; 	// spawning mesh position

	// particle set constants as global array
	ParticleSetConstants *psetdata;

	// Default Smoothing Function, its Gradient, and its Laplacian
	inline __host__ __device__ float DefaultKernel(const float dr, const float h){
		return ((315.0f/(64.0f*PI*h*h*h*h*h*h*h*h*h))*((h*h-dr*dr)*(h*h-dr*dr)*(h*h-dr*dr)));
	}
	inline __host__ __device__ float GradDefaultKernel(const float dr, const float h){
		return ((-945.0f/(32.0f*PI*h*h*h*h*h*h*h*h*h))*(h*h-dr*dr)*(h*h-dr*dr)*dr);
	}
	inline __host__ __device__ float LaplacDefaultKernel(const float dr, const float h){
		return (-945.0f/(32.0f*PI*h*h*h*h*h*h*h*h*h))*(h*h-dr*dr)*(3*h*h-7*dr*dr);
	}
	// Pressure Smoothing Function Gradient
	inline __host__ __device__ float GradPressureKernel(const float dr, const float h){
		return (-45.0f/(PI*h*h*h*h*h*h))*(h-dr)*(h-dr);
	}
	// Viscosity Smoothing Function Laplacian
	inline __host__ __device__ float LaplacViscosityKernel(const float dr, const float h){
		return (45.0f/(PI*h*h*h*h*h*h))*(h-dr);
	}
} g;

Fluidix<> *fx;

Particle *particleSet;

//////////////////////////////////////////////////
// Function Macros

// spawn model at given point with given scale
FUNC_EACH(spawn_model,
        p.r = g.mesh_min + p.r * (g.mesh_max - g.mesh_min);
)

// Spawn fluid and init particle density
FUNC_EACH(sph_spawn,
	p.r = make_xyz(rnd_uniform() * g.spawn_size.x, 		// fluid volume X size
				   rnd_uniform() * g.spawn_size.y,		// fluid volume Y size
				   rnd_uniform() * g.spawn_size.z)		// fluid volume Z size
		+ make_xyz(g.spawn_pos.x - g.spawn_size.x/2,	// fluid X position
				   g.spawn_pos.y - g.spawn_size.y/2,	// fluid Y position
				   g.spawn_pos.z - g.spawn_size.z/2);	// fluid Z position
)

// Init SPH values
FUNC_EACH(sph_init,
	// SPH initialize each particle density
    p.density = g.psetdata[p_set].particle_mass * g.DefaultKernel(0, g.psetdata[p_set].h);

	// re-initialize values
	p.tension = 0;
	p.normal = make_xyz(0, 0, 0);
)

// add density contribution from neighbors
FUNC_PAIR(sph_density,

	float d = g.psetdata[p1_set].particle_mass * g.DefaultKernel(dr, g.psetdata[p1_set].h);

    addFloat(p1.density, d);
    addFloat(p2.density, d);
)

// apply forces on pairs of nearby particles
// pressure and viscosity force
// surface normal and tension
FUNC_PAIR(sph_pair,
	// particle pressure = K(stiffness const) * density diff
    float p1_p = g.psetdata[p1_set].stiffness * (p1.density - g.psetdata[p1_set].density);
    float p2_p = g.psetdata[p2_set].stiffness * (p2.density - g.psetdata[p2_set].density);

    // F pressure:
    xyz vec = u * (g.psetdata[p1_set].particle_mass * g.GradPressureKernel(dr, g.psetdata[p1_set].h)
    		* -(p1_p + p2_p) / (2 * p2.density));

    // F viscosity
    vec += (p2.v - p1.v) * (g.psetdata[p1_set].viscosity * g.LaplacViscosityKernel(dr, g.psetdata[p1_set].h)
    		* g.psetdata[p1_set].particle_mass / p1.density);

    // add forces to the particles
    addVector(p1.f, vec);
    addVector(p2.f, -vec);

    // inward surface normal is the gradient of the color field (4.28)
    vec = u * (g.GradDefaultKernel(dr, g.psetdata[p1_set].h)
    		* g.psetdata[p1_set].particle_mass / p2.density);

    addVector(p1.normal, vec);
    addVector(p2.normal, -vec);

    // F surface tension, a part from (4.31)
    float tens = g.LaplacDefaultKernel(dr, g.psetdata[p1_set].h)
    		* g.psetdata[p1_set].particle_mass / p2.density;

    addFloat(p1.tension, tens);
    addFloat(p2.tension, -tens);
)

// apply forces on each particle
// gravity, buoyancy
FUNC_EACH(sph_each,
	// apply gravity on the particle, this is applied without dependencies on adjacent particles
	// it is acting equally on all fluid particles
    p.f.z -= GRAVITY * p.density;

	// TODO gravity is forgotten
	// p.f.y -= BUOYANCY * (p.density - DENSITY) * GRAVITY;
    p.f.z -= g.psetdata[p_set].buoyancy * (p.density - g.psetdata[p_set].density);

    // F surface tension
    // OBS threshold and magnitude is squared (4.32, 5.17 from kelager06)
    // (should be ||n.i|| >= sqrt(thresh), we have ||n.i||^2 >= thresh, seems fine)
    float normal_magnit_sq = xyz_lensq(p.normal);
    if (normal_magnit_sq > g.psetdata[p_set].threshold) {
    	// F surface tension, the other part of (4.31)
        p.f -= p.normal * (p.tension * g.psetdata[p_set].tension / sqrtf(normal_magnit_sq));
    }
)

// SPH Semi-Implicit Euler integration and re-initialization of values
FUNC_EACH(sph_euler_integrate,
	// integration
	p.v += p.f * DT / p.density;
	p.r += p.v * DT;

	// reset forces to zero being as the timestep ends
	p.f = make_xyz(0, 0, 0);

)

// Leapfrog Integration Step
FUNC_EACH(sph_leapfrog_integrate,
    p.v_hstep += (p.f / p.density) * DT;
	p.v = p.v_hstep + (p.f / p.density) * (DT / 2);
    p.r += p.v_hstep * DT;

	// reset forces to zero being as the timestep ends
    p.f = make_xyz(0, 0, 0);
)

// Object surface collision function
FUNC_SURFACE(surface_collision,
	// particle position is projected back along the surface normal (u)
	// with the penetration depth (dr)
	p.r = p.r + dr * u;

	// Force based
	// p.f += 5000 * u * dr;

	// Standard Hybrid Impulse-Projection method: (4.57)
	//p.v = p.v - (1 + g.psetdata[p_set].restitution) * (p.v % u) * u;

	// Standard Hybrid Impulse-Projection method + (4.58) introducing:
 	// ratio of the penetration depth to the distance between
	// the last particle position and the penetrating position

	/* Semi-Implicit Euler Integration*/
	p.v = p.v - (1 + (g.psetdata[p_set].restitution * dr) / (DT * xyz_len(p.v))) * (p.v % u) * u;
	/* Leap Frog Integration*/
//	p.v_hstep = p.v_hstep - (1 + (g.psetdata[p_set].restitution * dr)
//			/ (DT * xyz_len(p.v_hstep))) * (p.v_hstep % u) * u;

)

// Surface collision with "friction", restitution coefficient
FUNC_SURFACE(static_surface_collision,
	p.r = p.r + dr * u;
	/* Semi-Implicit Euler Integration*/
	p.v = p.v - (1 + (g.psetdata[p_set].restitution * dr) / (DT * xyz_len(p.v))) * (p.v % u) * u;

	p.v *= 0.9;
)

// Surface collision with ray-triangle intersection
FUNC_COLLISION(ray_triangle_collision,
		p.r = p.r + dr * u;
		p.v = p.v - (1 + (g.psetdata[p_set].restitution * dr) / (DT * xyz_len(p.v))) * (p.v % u) * u;
		printf("Force vector: %f %f %f \n",p.f.x, p.f.y, p.f.z);
)

// Collision with Liquid World Boundary
FUNC_EACH(world_boundary,
	// X coord
	// min boundary
    if (p.r.x < g.world_pos.x - (g.world_size.x/2)) {
    	/* Semi-Implicit Euler Integration*/
    	p.v.x = g.wall_elasticity * (g.world_pos.x - (g.world_size.x/2) - p.r.x) / DT;
    	/* Leap Frog Integration*/
    	//p.v_hstep.x = g.wall_elasticity * (g.world_pos.x - (g.world_size.x/2) - p.r.x) / DT;
    	p.r.x = g.world_pos.x - (g.world_size.x/2);
    }
	// max boundary
    if (p.r.x > g.world_pos.x + (g.world_size.x/2)) {
    	/* Semi-Implicit Euler Integration*/
    	p.v.x = g.wall_elasticity * (g.world_pos.x + (g.world_size.x/2)- p.r.x) / DT;
    	/* Leap Frog Integration*/
    	//p.v_hstep.x = g.wall_elasticity * (g.world_pos.x + (g.world_size.x/2)- p.r.x) / DT;
    	p.r.x = g.world_pos.x + (g.world_size.x/2);
    }

    // Y coord
	// min boundary
    if (p.r.y < g.world_pos.y - (g.world_size.y/2)) {
    	/* Semi-Implicit Euler Integration*/
    	p.v.y = g.wall_elasticity * (g.world_pos.y - (g.world_size.y/2) - p.r.y) / DT;
    	/* Leap Frog Integration*/
    	//p.v_hstep.y = g.wall_elasticity * (g.world_pos.y - (g.world_size.y/2) - p.r.y) / DT;
    	p.r.y = g.world_pos.y - (g.world_size.y/2);
    }
	// max boundary
    if (p.r.y > g.world_pos.y + (g.world_size.y/2)) {
    	/* Semi-Implicit Euler Integration*/
    	p.v.y = g.wall_elasticity * (g.world_pos.y + (g.world_size.y/2)- p.r.y) / DT;
    	/* Leap Frog Integration*/
    	//p.v_hstep.y = g.wall_elasticity * (g.world_pos.y + (g.world_size.y/2)- p.r.y) / DT;
    	p.r.y = g.world_pos.y + (g.world_size.y/2);
    }

    // Z coord
	// min boundary
    if (p.r.z < g.world_pos.z - (g.world_size.z/2)) {
    	/* Semi-Implicit Euler Integration*/
    	p.v.z = g.wall_elasticity * (g.world_pos.z - (g.world_size.z/2) - p.r.z) / DT;
    	/* Leap Frog Integration*/
    	//p.v_hstep.z = g.wall_elasticity * (g.world_pos.z - (g.world_size.z/2) - p.r.z) / DT;
    	p.r.z = g.world_pos.z - (g.world_size.z/2);
    }
	// max boundary
    if (p.r.z > g.world_pos.z + (g.world_size.z/2)) {
    	/* Semi-Implicit Euler Integration*/
    	p.v.z = g.wall_elasticity * (g.world_pos.z + (g.world_size.z/2)- p.r.z) / DT;
    	/* Leap Frog Integration*/
    	//p.v_hstep.z = g.wall_elasticity * (g.world_pos.z + (g.world_size.z/2)- p.r.z) / DT;
    	p.r.z = g.world_pos.z + (g.world_size.z/2);
    }
)

//////////////////////////////////////////////////
FluidEngine::FluidEngine()
{
	// Initialize Fluidix
    fx = new Fluidix<>(&g);

    // Initialize members
    worldBoundariesCreated = false;

    // Init global arrays
    fx->createGlobalArray(&g.psetdata, MAX_NR_SETS * sizeof(ParticleSetConstants));
}

//////////////////////////////////////////////////
FluidEngine::~FluidEngine()
{
// TODO if destructor is called before last CUDA update it crashes badly
//	delete fx;
}

//////////////////////////////////////////////////
void FluidEngine::Init()
{

}

//////////////////////////////////////////////////
void FluidEngine::Update()
{
    fx->setTimer();

    for (FluidSetMap::iterator fluid_iter = this->fluidSetMap.begin();
            fluid_iter != this->fluidSetMap.end(); fluid_iter++)
    {
    	// Init SPH values
    	fx->runEach(sph_init(),
    			fluid_iter->second->GetParticleSetId()); 				// particle Set of the Fluid

    	// add density contribution from neighbors
    	fx->runPair(sph_density(),
    			fluid_iter->second->GetParticleSetId(), 				// particle Set of the Fluid
    			fluid_iter->second->GetParticleSetId(), 				// particle Set of the Fluid
    			g.psetdata[fluid_iter->second->GetParticleSetId()].h); 	// smoothing length of of the Fluid

    	// apply forces on pairs of nearby particles, pressure, viscosity and surface tension force
    	fx->runPair(sph_pair(),
    			fluid_iter->second->GetParticleSetId(), 				// particle Set of the Fluid
    			fluid_iter->second->GetParticleSetId(), 				// particle Set of the Fluid
    			g.psetdata[fluid_iter->second->GetParticleSetId()].h);	// smoothing length of the Fluid

    	// apply forces on each particle, gravity and buoyancy
    	fx->runEach(sph_each(),
    			fluid_iter->second->GetParticleSetId()); 		// particle Set of the Fluid



    	// if world boundaries are created interact with them
    	if (this->worldBoundariesCreated)
    	{
    		fx->runEach(world_boundary(),
    				fluid_iter->second->GetParticleSetId()); 	// particle Set of the Fluid
    	}

        // interact with all STATIC Objects
        for (ObjectSetMap::iterator object_iter = this->objectSetMap.begin();
                object_iter != this->objectSetMap.end(); object_iter++)
        {
            // apply surface collision with static object
            fx->runSurface(static_surface_collision(),
                    object_iter->second->GetLinkId(), 			// link Set of the Object
                    fluid_iter->second->GetParticleSetId(),		// particle Set of the Object
                    -1);										// -1 = interact with all depth sizes

        }

        // interact with all MOVABLE Objects
        for (ObjectSetMap::iterator movable_object_iter = this->movableObjectSetMap.begin();
                movable_object_iter != this->movableObjectSetMap.end(); movable_object_iter++)
        {
//        	fx->runCollision(ray_triangle_collision(),
//                    movable_object_iter->second->GetLinkId(), 	// link Set of the Movable Object
//                    fluid_iter->second->GetParticleSetId(),		// particle Set of the Movable Object
//                    DT);										// time step for the ray start point r-v*dt

            // apply surface collision with movable object
            fx->runSurface(surface_collision(),
                    movable_object_iter->second->GetLinkId(), 	// link Set of the Movable Object
                    fluid_iter->second->GetParticleSetId(),		// particle Set of the Movable Object
                    -1);										// -1 = interact with all depth sizes
        }

    	// Semi Impliciy Euler integration
    	fx->runEach(sph_euler_integrate(),
    			fluid_iter->second->GetParticleSetId()); 		// particle Set of the Fluid

    	// Leapfrog integration
    	// fx->runEach(sph_leapfrog_integrate(),
    	// fluid_iter->second->GetParticleSetId()); 			// particle Set of the Fluid

    	//TODO hardcoded testing purposes
    	//fx->applyParticleArray(1);
    }

    // get the durration of the computation in order to compare it to the real time factor
    float comp_durr = fx->getTimer();

    // if faster than the real time factor requested
    if ((DT_MS / comp_durr) > 1)
    {
        // sleep for the remaining nanoseconds, hence the '*1000' from milis -> nanos
    	usleep( (DT_MS - comp_durr) * 1000 / REAL_TIME_FACTOR);
    }

    comp_durr = fx->getTimer();
    printf("*GPU* Update dur: %.2f ms; Real Time Factor: %.2f \n", comp_durr, (float) DT_MS / comp_durr);
}

//////////////////////////////////////////////////
//void FluidEngine::CreateWorldBoundaries(float3 _pos, float3 _size, float _elasticity)
void FluidEngine::CreateWorldBoundaries(float _pos_x,
											float _pos_y,
											float _pos_z,
											float _size_x,
											float _size_y,
											float _size_z,
											float _elasticity)
{
	// set flag in order to compute collisions with the world boundaries
	this->worldBoundariesCreated = true;

	// Set world position
	g.world_pos = make_xyz(_pos_x, _pos_y, _pos_z);

	// Set world boundary size
	g.world_size = make_xyz(_size_x, _size_y, _size_z);

	// Set hard wall boundary elasticity
	g.wall_elasticity = _elasticity;
}

//////////////////////////////////////////////////
void FluidEngine::AddFluidSet(float _spawn_pos_x,
								float _spawn_pos_y,
								float _spawn_pos_z,
								float _size_x,
								float _size_y,
								float _size_z,
								int _nrNeighbors,
								float _particleSize,
								float _massDensity,
								float _stiffness,
								float _viscosity,
								float _buoyancy,
								float _surfaceTension)
{
	int particle_nr;
	float h;

	// local instance of the fluid class
	fluidix::FluidSet *fluidSet =  new fluidix::FluidSet();

	// set fluid volume (m3)
	fluidSet->SetVolume(_size_x * _size_y * _size_z);

	// set particle size (m) as side of a cube
	fluidSet->SetParticleSize(_particleSize);

	// compute and set particle numbers
	particle_nr = fluidSet->GetVolume() / fluidSet->GetParticleVolume();
	fluidSet->SetParticleNr(particle_nr);

	// set avg number of neighbors (for smoothing length)
	fluidSet->SetSmoothingNeighborsNr(_nrNeighbors);

	// set smoothing length
	h = powf(3.0f* fluidSet->GetVolume() * fluidSet->GetSmoothingNeighboursNr() /
			(4.0f * PI * fluidSet->GetParticleNr()), 0.333f);
	fluidSet->SetSmoothingLength(h);

	// set fluid density
	fluidSet->SetDensity(_massDensity);

	// set fluid stiffness
	fluidSet->SetStiffness(_stiffness);

	// set fluid viscosity
	fluidSet->SetViscosity(_viscosity);

	// set fluid buoyancu
	fluidSet->SetBuoyancy(_buoyancy);

	// set fluid surface tension
	fluidSet->SetSurfaceTension(_surfaceTension);

	// set fluid surface tension threshold
	fluidSet->SetSurfTensThreshold(_massDensity / _nrNeighbors);

	// set particle mass
	fluidSet->SetParticleMass(fluidSet->GetDensity() * fluidSet->GetVolume() /
			fluidSet->GetParticleNr());


	// get unique Id and create particle set
	int unique_id = fx->createParticleSet(particle_nr);

	// set ParticleSet unique ID
	fluidSet->SetParticleSetId(unique_id);

	// add particle set to the map
	this->fluidSetMap[unique_id] = fluidSet;


	// Add the Fluid parameters to the global array
	g.psetdata[unique_id].particle_mass = fluidSet->GetParticleMass();

	g.psetdata[unique_id].stiffness = fluidSet->GetStiffness();

	g.psetdata[unique_id].density = fluidSet->GetDensity();

	g.psetdata[unique_id].viscosity = fluidSet->GetViscosity();

	g.psetdata[unique_id].buoyancy = fluidSet->GetBuoyancy();

	g.psetdata[unique_id].tension = fluidSet->GetSurfaceTension();

	g.psetdata[unique_id].threshold = fluidSet->GetSurfTensThreshold();

	g.psetdata[unique_id].h = fluidSet->GetSmoothingLength();

	fx->applyGlobalArray(&g.psetdata);

	// set spawning position
    g.spawn_pos = make_xyz(_spawn_pos_x, _spawn_pos_y, _spawn_pos_z);

    // set spawning area
    g.spawn_size  = make_xyz(_size_x, _size_y, _size_z);

    // Spawn fluid and init particle density for SPH
    fx->runEach(sph_spawn(), unique_id);

    // Print Info about the Fluid
    std::cout << "** Fluid set nr: " << unique_id <<", parameters:" << std::endl;
    std::cout << "Particle Nr: " << particle_nr << std::endl;
    std::cout << "Neighbours: " << _nrNeighbors << std::endl;
    std::cout << "Particle Mass: " << fluidSet->GetParticleMass() << std::endl;
    std::cout << "Stiffness: " << fluidSet->GetStiffness() << std::endl;
    std::cout << "Mass Density: " << fluidSet->GetDensity() << std::endl;
    std::cout << "Viscosity: " << fluidSet->GetViscosity() << std::endl;
    std::cout << "Buoyancy: " << fluidSet->GetBuoyancy() << std::endl;
    std::cout << "Surface Tension: " << fluidSet->GetSurfaceTension() << std::endl;
    std::cout << "Surface Tension Threshold (squared): " << fluidSet->GetSurfTensThreshold() << std::endl;
    std::cout << "Smoothing length (h): " << fluidSet->GetSmoothingLength() << std::endl;

}

//////////////////////////////////////////////////
FluidSet FluidEngine::GetFluidSet(int _id)
{
	return *this->fluidSetMap.find(_id)->second;
}

//////////////////////////////////////////////////
void FluidEngine::AddObject(std::string _path,
							float _spawn_pos_x,
							float _spawn_pos_y,
							float _spawn_pos_z,
							float _scale_x,
							float _scale_y,
							float _scale_z,
							float _restitution_coeff)
{
    // local instance of the object class
    fluidix::ObjectSet *objectSet = new fluidix::ObjectSet();

    // import model and set particle/link set unique IDs
    int2 unique_ids = fx->importModel(_path.c_str());

    // set Objects Particle and Link Set IDs
    // .x = particle set index , .y link set index
    objectSet->SetParticleAndLinkSetId(unique_ids.x, unique_ids.y);

    // save the initial position of the object
    objectSet->SetWorldPosition(_spawn_pos_x, _spawn_pos_y, _spawn_pos_z);

    // TODO hardcoded
    // save the initial orientation of the object
    objectSet->SetWorldOrientation(1,0,0,0);

    // set restitution coefficient, // 1 - elastic collision , 0 - inelastic
    objectSet->SetRestitutionCoef(_restitution_coeff); //TODO hardcoded

    // add Object to the map, unique Id is the Particle Set ID
    this->objectSetMap[unique_ids.x] = objectSet;

    // set the restitution coefficient for this object
	g.psetdata[unique_ids.x].restitution = objectSet->GetRestitutionCoef();
	fx->applyGlobalArray(&g.psetdata);


    // Set mesh spawning position
    g.mesh_min = make_xyz(_spawn_pos_x, _spawn_pos_y, _spawn_pos_z);
    g.mesh_max = make_xyz(_spawn_pos_x + _scale_x, _spawn_pos_y + _scale_y, _spawn_pos_z + _scale_z);

    // .x = particle set index , .y link set index
    fx->runEach(spawn_model(), unique_ids.x);
}

//////////////////////////////////////////////////
void FluidEngine::AddMovableObject(std::string _path,
									float _spawn_pos_x,
									float _spawn_pos_y,
									float _spawn_pos_z,
									float _scale_x,
									float _scale_y,
									float _scale_z,
									float _restitution_coeff)
{
    // local instance of the object class
    fluidix::ObjectSet *objectSet = new fluidix::ObjectSet();

    // import model and set particle/link set unique IDs
    int2 unique_ids = fx->importModel(_path.c_str());

    // set Objects Particle and Link Set IDs
    // .x = particle set index , .y link set index
    objectSet->SetParticleAndLinkSetId(unique_ids.x, unique_ids.y);

    // save the initial position of the object
    objectSet->SetWorldPosition(_spawn_pos_x, _spawn_pos_y, _spawn_pos_z);

    // TODO hardcoded
    // save the initial orientation of the object
    objectSet->SetWorldOrientation(1,0,0,0);

    // set restitution coefficient, // 1 - elastic collision , 0 - inelastic
    objectSet->SetRestitutionCoef(_restitution_coeff);

    // add Object to the map, unique Id is the Particle Set ID
    this->movableObjectSetMap[unique_ids.x] = objectSet;

    // set the restitution coefficient for this object
	g.psetdata[unique_ids.x].restitution = objectSet->GetRestitutionCoef();
	fx->applyGlobalArray(&g.psetdata);


    // Set mesh spawning position
    g.mesh_min = make_xyz(_spawn_pos_x, _spawn_pos_y, _spawn_pos_z);
    g.mesh_max = make_xyz(_spawn_pos_x + _scale_x, _spawn_pos_y + _scale_y, _spawn_pos_z + _scale_z);

    // .x = particle set index , .y link set index
    fx->runEach(spawn_model(), unique_ids.x);
}

//////////////////////////////////////////////////
void FluidEngine::GetParticlePositions(int _setId,
				std::vector<float> &_particlePositions_x,
				std::vector<float> &_particlePositions_y,
				std::vector<float> &_particlePositions_z)
{
    // get the particle set
    particleSet = fx->getParticleArray(_setId);

    // get the nr of particles
    int p_count = fx->getParticleCount(_setId);

    // write every particle postition to the vector
    for (int p_i = 0; p_i < p_count; ++p_i)
    {
    	_particlePositions_x[p_i] = particleSet[p_i].r.x / 10.0f;
    	_particlePositions_y[p_i] = particleSet[p_i].r.y / 10.0f;
    	_particlePositions_z[p_i] = particleSet[p_i].r.z / 10.0f;
    }
}

//////////////////////////////////////////////////
int FluidEngine::GetFluidSetCount()
{
	return this->fluidSetMap.size();
}

//////////////////////////////////////////////////
int FluidEngine::GetObjectSetCount()
{
    return this->objectSetMap.size();
}

//////////////////////////////////////////////////
int FluidEngine::GetMovableObjectSetCount()
{
    return this->movableObjectSetMap.size();
}

//////////////////////////////////////////////////
int FluidEngine::GetParticleCount(int _setId)
{
	return fx->getParticleCount(_setId);
}

//////////////////////////////////////////////////
std::vector<int> FluidEngine::GetFluidSetIDs()
{
	std::vector<int> IDs;

    for (FluidSetMap::iterator iter = this->fluidSetMap.begin();
    		iter != this->fluidSetMap.end(); iter++)
    {
    	IDs.push_back(iter->second->GetParticleSetId());
    }

	return IDs;
}

//////////////////////////////////////////////////
std::vector<int> FluidEngine::GetObjectParticleSetIDs()
{
    std::vector<int> IDs;

    for (ObjectSetMap::iterator iter = this->objectSetMap.begin();
            iter != this->objectSetMap.end(); iter++)
    {
        IDs.push_back(iter->second->GetParticleSetId());
    }

    return IDs;
}

//////////////////////////////////////////////////
std::vector<int> FluidEngine::GetMovableObjectParticleSetIDs()
{
    std::vector<int> IDs;

    for (ObjectSetMap::iterator iter = this->movableObjectSetMap.begin();
            iter != this->movableObjectSetMap.end(); iter++)
    {
        IDs.push_back(iter->second->GetParticleSetId());
    }

    return IDs;
}

//////////////////////////////////////////////////
void FluidEngine::SetObjectPosition(int _setId,
										float _position_x,
										float _position_y,
										float _position_z)
{
    // get the particle set
    particleSet = fx->getParticleArray(_setId);

    // create fluidix position from arguments
    float4 _position = make_xyz(_position_x, _position_y, _position_z);

    // calculate the relative position
    float4 world_pos;
    this->movableObjectSetMap[_setId]->GetWorldPosition(world_pos.x,world_pos.y,world_pos.z);
    float4 diff = _position - world_pos;

    // get number of particles
    int p_count = fx->getParticleCount(_setId);

    // iterate through the particle set
    for (int i = 0; i < p_count; ++i)
    {
        particleSet[i].r += diff;
    }

    // update object new position
    this->movableObjectSetMap[_setId]->SetWorldPosition(_position.x,_position.y,_position.z);

    // apply changes made to particles outside of an interaction
    fx->applyParticleArray(_setId);

}

//////////////////////////////////////////////////
void FluidEngine::SetObjectOrientation(int _setId,
											float _quat_w,
											float _quat_x,
											float _quat_y,
											float _quat_z)
{
	// get the world position, to translate it to the center and then back
	float4 world_pos;
	this->movableObjectSetMap[_setId]->GetWorldPosition(world_pos.x,world_pos.y,world_pos.z);

	// create a fluidix quat from the arguments
	fluidix::Quaternion _quat = fluidix::Quaternion(_quat_w, _quat_x, _quat_y, _quat_z);

	// the new quat * the inverse of the last position so it takes it back to the init position
	fluidix::Quaternion quat;
	this->movableObjectSetMap[_setId]->GetWorldOrientation(quat.w,quat.x,quat.y,quat.z);
	quat = _quat * quat.GetInverse();

    // get the particle set
    particleSet = fx->getParticleArray(_setId);

    // get number of particles
    int p_count = fx->getParticleCount(_setId);

    // iterate through the particle set
    for (int i = 0; i < p_count; ++i)
    {
    	// rotate the translated position vector
    	particleSet[i].r = quat.RotateVector(particleSet[i].r - world_pos);

    	// translate back the position vector by adding back its original position
    	particleSet[i].r += world_pos;

    }

    // update object new position
    this->movableObjectSetMap[_setId]->SetWorldOrientation(_quat.w,_quat.x,_quat.y,_quat.z);

    // apply changes made to particles outside of an interaction
    fx->applyParticleArray(_setId);

}

//////////////////////////////////////////////////
void FluidEngine::SetObjectOrientation(int _setId, float _r, float _p, float _y)
{
	Quaternion quat(_r, _p, _y);

	this->SetObjectOrientation(_setId, quat.w, quat.x, quat.y, quat.z);
}

//////////////////////////////////////////////////
void FluidEngine::SetObjectPose(int _setId,
								float _position_x,
								float _position_y,
								float _position_z,
								float _quat_w,
								float _quat_x,
								float _quat_y,
								float _quat_z)
{
	// get the world position, to translate it to the center and then back
	float4 world_pos;
	this->movableObjectSetMap[_setId]->GetWorldPosition(world_pos.x,world_pos.y,world_pos.z);

    // create fluidix position from arguments
    float4 _position = make_xyz(_position_x, _position_y, _position_z);

	// create a fluidix quat from the arguments
	fluidix::Quaternion _quat = fluidix::Quaternion(_quat_w, _quat_x, _quat_y, _quat_z);

	// the new quat * the inverse of the last position so it takes it back to the init position
	fluidix::Quaternion quat;
	this->movableObjectSetMap[_setId]->GetWorldOrientation(quat.w,quat.x,quat.y,quat.z);
	quat = _quat * quat.GetInverse();

	// get the particle set
    particleSet = fx->getParticleArray(_setId);

    // get number of particles
    int p_count = fx->getParticleCount(_setId);

    // iterate through the particle set
    for (int i = 0; i < p_count; ++i)
    {
    	// rotate the translated position vector
    	particleSet[i].r = quat.RotateVector(particleSet[i].r - world_pos);

    	// translate back the position to the sensor value
    	particleSet[i].r += _position;
    }

    // update object new position
    this->movableObjectSetMap[_setId]->SetWorldPosition(_position.x, _position.y, _position.z);

    // update object new position
    this->movableObjectSetMap[_setId]->SetWorldOrientation(_quat.w,_quat.x,_quat.y,_quat.z);

    // apply changes made to particles outside of an interaction
    fx->applyParticleArray(_setId);
}




