/* Desc: Object Set class
 * Author: Andrei Haidu
 * Date: 11 Jul. 2013
 */

#ifndef OBJECT_SET_HH
#define OBJECT_SET_HH

/// \brief namespace for fluids
namespace fluidix
{
    /// \class ObjectSet ObjectSet.hh
    /// \brief ObjectSet class
    class ObjectSet
    {
        /// \brief Constructor
        public: ObjectSet();

        /// \brief Destructor
        public: virtual ~ObjectSet();

        /// \brief Load
        public: virtual void Load();

        /// \brief Finalize
        public: virtual void Fini();

        /// \brief Initialize
        public: virtual void Init();

        /// \brief Reset
        public: virtual void Reset();

        /// \brief Update
        public: void Update();

        /// \brief Set particle set ID
        public: void SetParticleSetId(int _id);

        /// \brief Get particle set ID
        public: int GetParticleSetId();

        /// \brief Set Link set ID
        public: void SetLinkSetId(int _linkSetId);

        /// \brief Return ID of the Link set
        public: int GetLinkId();

        /// \brief Set ID of the Particle and Link Set
        public: void SetParticleAndLinkSetId(int _particleSetId, int _linkSetId);

        /// \brief Set object restitution coeficient
        public: void SetRestitutionCoef(float _coef);

        /// \brief Get object restitution coeficient
        public: float GetRestitutionCoef();

        /// \brief Set object world position
        public: void SetWorldPosition(float _x, float _y, float _z);

        /// \brief Get object world position with references
        public: void GetWorldPosition(float &_pos_x, float &_pos_y, float &_pos_z);

        /// \brief Set object world orientation
        public: void SetWorldOrientation(float _quat_w,
        		float _quat_x, float _quat_y, float _quat_z);

        /// \brief Get object world orientation with references
        public: void GetWorldOrientation(float &_quat_w,
        		float &_quat_x, float &_quat_y, float &_quat_z);

        /// \brief World position of the object
        protected: float posX, posY, posZ;

        /// \brief World orientation of the object
        protected: float quatW, quatX, quatY, quatZ;

        /// \brief Unique ID of the particle Set
        protected: int particleSetId;

        /// \brief ID of the link set
        protected: int linkSetId;

        /// \brief Object restitution coeficient, 1 - elastic collision , 0 - inelastic
        protected: float restitutionCoef;

    };
}
#endif

