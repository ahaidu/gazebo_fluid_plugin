/* Desc: System plugin for rendering the particles from Fluidix
 * Author: Andrei Haidu
 * Date: 11 Jul. 2013
 */

#ifndef FLUID_PLUGIN_HH
#define FLUID_PLUGIN_HH

#include <gazebo.hh>
#include "rendering/rendering.hh"
#include "gui/Gui.hh"
#include "common/Events.hh"
#include "common/Time.hh"

#include "boost/thread.hpp"
#include "boost/bind.hpp"
#include "boost/smart_ptr.hpp"

#include <OGRE/OgreRoot.h>

#include "FluidEngine.hh"

#include "gazebo/transport/TransportTypes.hh"
#include "gazebo/msgs/MessageTypes.hh"

namespace gazebo
{

	class FluidPlugin : public SystemPlugin
	{

	public: FluidPlugin();

	public: ~FluidPlugin();

	protected: void Load(int /*_argc*/, char ** /*_argv*/);

	protected: void Init();

	protected: void OnUpdate();

    /// \brief Update spheres position. A thread uses this function
    /// to call Fluidix to compute the positions
    private: void UpdateFluid();

    private: void InitFluidRendering();

    private: void InitObjectRendering();

    private: void InitMovableObjectRendering();

    /// \brief Create Ogre rendering entities for the Fluid particles
    private: void CreateFluidRenderingEntities(
            int _index, int _setID, float _sphereSize, std::string _material);

    /// \brief Create Ogre rendering entities for the Object particles
    private: void CreateObjectRenderingEntities(
            int _index, int _setID, float _sphereSize, std::string _material);

    /// \brief Create Ogre rendering entities for the Movable Object particles
    private: void CreateMovableObjectRenderingEntities(
            int _index, int _setID, float _sphereSize, std::string _material);

    /// \brief Change particle positions and for rendering
    private: void RenderParticles();

    /// \brief Initialize and set up the world control message node
    private: void InitWorldControlNode();

    /// \brief Send  world control message with pausing the world
    private: void PauseWorld();

    /// \brief Send  world control message with making a simulation step
    private: void StepWorld();

    /// \brief Node used for communication.
    private: transport::NodePtr node;

    /// \brief Used to start, stop, and step simulation.
    private: transport::PublisherPtr worldControlPub;

    /// \brief world name
    private: std::string worldName;

    // plugin variables
    private: event::ConnectionPtr updateConnection;
    private: boost::thread* updateThread;
    private: fluidix::FluidEngine* fluidEngine;
    private: Ogre::SceneManager* mSceneMgr;
    private: rendering::VisualPtr visualModelPtr;

    // fluid ogre stuff
    private: std::vector<std::vector<Ogre::Entity*> > fluidEntities2D;
    private: std::vector<std::vector<Ogre::SceneNode*> > fluidSceneNodes2D;
//    private: std::vector<std::vector<float3> > fluidParticlesPositions2D;
    private: std::vector<std::vector<float> > fluidParticlesPositions2D_x;
    private: std::vector<std::vector<float> > fluidParticlesPositions2D_y;
    private: std::vector<std::vector<float> > fluidParticlesPositions2D_z;
    private: std::vector<int> fluidSetIDs;

    // static object ogre stuff
    private: std::vector<std::vector<Ogre::Entity*> > objectEntities2D;
    private: std::vector<std::vector<Ogre::SceneNode*> > objectSceneNodes2D;
//    private: std::vector<std::vector<float3> > objectParticlesPositions2D;
    private: std::vector<std::vector<float> > objectParticlesPositions2D_x;
    private: std::vector<std::vector<float> > objectParticlesPositions2D_y;
    private: std::vector<std::vector<float> > objectParticlesPositions2D_z;
    private: std::vector<int> objectParticleSetIDs;

    // movable object ogre stuff
    private: std::vector<std::vector<Ogre::Entity*> > movableObjectEntities2D;
    private: std::vector<std::vector<Ogre::SceneNode*> > movableObjectSceneNodes2D;
//    private: std::vector<std::vector<float3> > movableObjectParticlesPositions2D;
    private: std::vector<std::vector<float> > movableObjectParticlesPositions2D_x;
    private: std::vector<std::vector<float> > movableObjectParticlesPositions2D_y;
    private: std::vector<std::vector<float> > movableObjectParticlesPositions2D_z;
    private: std::vector<int> movableObjectParticleSetIDs;

    private: bool pause_flag;

	};
}
#endif
