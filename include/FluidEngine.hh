#ifndef FLUID_ENGINE_HH
#define FLUID_ENGINE_HH

#include <vector>
#include <map>
#include <string>
#include <list>

#include "ObjectSet.hh"
#include "FluidSet.hh"

namespace fluidix
{

    typedef std::map<int, fluidix::FluidSet*> FluidSetMap;

    typedef std::map<int, fluidix::ObjectSet*> ObjectSetMap;

    /// \brief FluidEngine class
	class FluidEngine
	{

	/// \brief Constructor
    public: FluidEngine();

    /// \brief Destructor
    public: ~FluidEngine();

    /// \brief Initialize FluidEngineix
    public: void Init();

    /// \brief Update liquid simulation with a new time-step
    public: void Update();

    /// \brief Create a Liquid World with boundary collisions
    public: void CreateWorldBoundaries(float _pos_x,
										float _pos_y,
										float _pos_z,
										float _size_x,
										float _size_y,
										float _size_z,
										float _elasticity);

    /// \brief Create a new fluid set in the given position, spawned in the given area
    public: void AddFluidSet(float _spawn_pos_x,
    							float _spawn_pos_y,
    							float _spawn_pos_z,
    							float _size_x,
    							float _size_y,
    							float _size_z,
    							int _nrNeighbors,
    							float _particleSize,
    							float _massDensity,
    							float _stiffness,
    							float _viscosity,
    							float _buoyancy,
    							float _surfaceTension);

    /// \brief Return fluid set
    public: FluidSet GetFluidSet(int _id);

    /// \brief Add an Object to the simulation
    public: void AddObject(std::string _path,
							float _spawn_pos_x,
							float _spawn_pos_y,
							float _spawn_pos_z,
							float _scale_x,
							float _scale_y,
							float _scale_z,
							float _restitution_coeff);

    /// \brief Add a Movable Object to the simulation
    public: void AddMovableObject(std::string _path,
									float _spawn_pos_x,
									float _spawn_pos_y,
									float _spawn_pos_z,
									float _scale_x,
									float _scale_y,
									float _scale_z,
									float _restitution_coeff);

    /// \brief Return particle positions for the given set
    public: void GetParticlePositions(int _setId,
				std::vector<float> &_particlePositions_x,
				std::vector<float> &_particlePositions_y,
				std::vector<float> &_particlePositions_z);

    /// \brief Return number of Fluid sets
    public: int GetFluidSetCount();

    /// \brief Return number of Object sets
    public: int GetObjectSetCount();

    /// \brief Return number of Movable Object sets
    public: int GetMovableObjectSetCount();

    /// \brief Return particle number for the given set
    public: int GetParticleCount(int _setId);

    /// \brief Retun a vector with all the IDs of the Fluid sets
    public: std::vector<int> GetFluidSetIDs();

    /// \brief Retun a vector with all the IDs of the Object sets
    public: std::vector<int> GetObjectParticleSetIDs();

    /// \brief Retun a vector with all the IDs of the Object sets
    public: std::vector<int> GetMovableObjectParticleSetIDs();

    /// \brief Set object's position
    public: void SetObjectPosition(int _setId,
									float _position_x,
									float _position_y,
									float _position_z);

    /// \brief Set object's orientation from quaternion
    public: void SetObjectOrientation(int _setId,
										float _quat_w,
										float _quat_x,
										float _quat_y,
										float _quat_z);

    /// \brief Set object's orientation from euler angles
    public: void SetObjectOrientation(int _setId,
										float _r,
										float _p,
										float _y);

    /// \brief Set object's position
    public: void SetObjectPose(int _setId,
								float _position_x,
								float _position_y,
								float _position_z,
								float _quat_w,
								float _quat_x,
								float _quat_y,
								float _quat_z);


    /// \brief Flag if liquid world is created in order to compute boundary collisions
    private: bool worldBoundariesCreated;

    private: FluidSetMap fluidSetMap;

    private: ObjectSetMap objectSetMap;

    private: ObjectSetMap movableObjectSetMap;

	};
}
#endif
