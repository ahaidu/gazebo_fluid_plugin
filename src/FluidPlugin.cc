/* Desc: System plugin for rendering the particles from Fluidix
 * Author: Andrei Haidu
 * Date: 11 Jul. 2013
 */

#include "FluidPlugin.hh"
#include <cmath>

using namespace gazebo;

#define BOX_PATH "/home/haidu/sandbox/gazebo_fluid_plugin/meshes/box.stl"
#define BOX45_PATH "/home/haidu/sandbox/gazebo_fluid_plugin/meshes/box45.stl"
#define SPHERE_PATH "/home/haidu/sandbox/gazebo_fluid_plugin/meshes/sphere.stl"
#define CYLINDER_PATH "/home/haidu/sandbox/gazebo_fluid_plugin/meshes/cylinder.stl"
#define CONE_PATH "/home/haidu/sandbox/gazebo_fluid_plugin/meshes/cone.stl"
#define MUG_PATH "/home/haidu/sandbox/gazebo_fluid_plugin/meshes/mug.stl"
#define THICK_MUG_PATH "/home/haidu/sandbox/gazebo_fluid_plugin/meshes/thick_mug.stl"

#define PI 3.14159265359

#define UPDATE_THREAD_SLEEP 20 // update thread sleep in milliseconds


//////////////////////////////////////////////////
FluidPlugin::FluidPlugin()
{
	// initialize Fluidix
    this->fluidEngine = new fluidix::FluidEngine();

    this->pause_flag = false;
}

//////////////////////////////////////////////////
FluidPlugin::~FluidPlugin()
{
    delete this->fluidEngine;
}

//////////////////////////////////////////////////
void FluidPlugin::Load(int /*_argc*/, char ** /*_argv*/)
{
	// Create World collision bondaries
	this->fluidEngine->CreateWorldBoundaries(0.0f, 0.0f, 5.0f,		// center position (xyz)
											10.0f, 10.0f, 10.0f,	// size 	(xyz)
											0.5f);					// elasticity

	/* Spawn water like fluid*/
	this->fluidEngine->AddFluidSet(0.0, 0.0, 2.0, 					// center position (xyz)
									0.3f, 0.3f, 0.3f,				// volume size (xyz)
									20,								// nr neighbors
									0.02716f,						// particle size / cube side (m) / 10
									998.29f,						// mass density
									3.0f,							// stiffness
									3.5f,							// viscosity
									0.0f,							// buoyancy
									0.0728f);						// surface tension


	// Movable Object
	this->fluidEngine->AddMovableObject(BOX_PATH,
									   0.0, 0.0, 0.0,				// position (xyz)
									   0.5, 0.5, 0.5,				// size (xyz)
									   0.05);						// restitution coefficient

	// Pancake Maker Static Object
	this->fluidEngine->AddObject(MUG_PATH,
								0, 0, 1.0,							// position (xyz)
								0.5, 0.5, 0.5,						// size (xyz)
								0.05);								// restitution coefficient

}

//////////////////////////////////////////////////
void  FluidPlugin::Init()
{
	// Set world name, can only be done in or after Init(), when having access to gui;
	this->worldName = gui::get_active_camera(
				)->GetScene() ->GetName();

    // Set scene manager, has to be set in Init() (or later)
    this->mSceneMgr = gui::get_active_camera(
    			)->GetScene()->GetManager();

    // initialize Gazebo world control transport node
    //FluidPlugin::InitWorldControlNode();

    // send world control msg to pause the simulation
    //FluidPlugin::PauseWorld();

    // Set up the rendering entities;
    FluidPlugin::InitFluidRendering();
    FluidPlugin::InitObjectRendering();
    FluidPlugin::InitMovableObjectRendering();


    // Create a thread to compute spheres position via Fluidix
    this->updateThread = new boost::thread(boost::bind(
            &FluidPlugin::UpdateFluid, this));

	// Listen to the update event.
	// This event is broadcast every simulation iteration.
	this->updateConnection = event::Events::ConnectPreRender(
			boost::bind(&FluidPlugin::OnUpdate, this));
}

//////////////////////////////////////////////////
void FluidPlugin::OnUpdate()
{
    // Get scene pointer
    rendering::ScenePtr scene = rendering::get_scene();

    // follow the given object from gazebo in fluidix
    if( scene->GetVisual("fluid_mug"))
    {
    	math::Pose mug_pose = scene->GetVisual("fluid_mug")->GetPose();

    	// set object pose
    	int movableSetId = this->fluidEngine->GetMovableObjectParticleSetIDs().front();
    	// position *10 for scaling the liquid
    	this->fluidEngine->SetObjectPose(movableSetId,
										mug_pose.pos.x*10,			// position (xyz)
										mug_pose.pos.y*10,
										mug_pose.pos.z*10,
										mug_pose.rot.w,				// orientation quat (wxyz)
										mug_pose.rot.x,
										mug_pose.rot.y,
										mug_pose.rot.z);
    }

	// Render particles with the updated positions
	FluidPlugin::RenderParticles();
}

//////////////////////////////////////////////////
void FluidPlugin::UpdateFluid()
{
    while(true)
    {

    	if (!this->pause_flag)
    	{
			// call the Fluidix function to update the particles interaction
			this->fluidEngine->Update();
    	}

    	// Send a simulation step req in Gazebo
        //FluidPlugin::StepWorld();

    }

}

//////////////////////////////////////////////////
void  FluidPlugin::InitFluidRendering()
{
    // get the vector of all IDs of fluid particle sets
    this->fluidSetIDs = this->fluidEngine->GetFluidSetIDs();

    // for all Fluid sets create the rendering entities
    for (int i = 0; i < this->fluidEngine->GetFluidSetCount(); ++i)
    {
        // initialise the size of the 2d vector with all particles positions
//        this->fluidParticlesPositions2D.push_back(
//                std::vector<float3>(this->fluidEngine->GetParticleCount(
//                        this->fluidSetIDs[i])));

        this->fluidParticlesPositions2D_x.push_back(
                std::vector<float>(this->fluidEngine->GetParticleCount(
                        this->fluidSetIDs[i])));
        this->fluidParticlesPositions2D_y.push_back(
                std::vector<float>(this->fluidEngine->GetParticleCount(
                        this->fluidSetIDs[i])));
        this->fluidParticlesPositions2D_z.push_back(
                std::vector<float>(this->fluidEngine->GetParticleCount(
                        this->fluidSetIDs[i])));

        // Create the rendering entities
        // has to be called in Init() (or later) after the SceneMgr is initialized
        // scale of 0.001 = 1m, one zero is for scaling down with 10 the whole fluidsim
        this->CreateFluidRenderingEntities(i, this->fluidSetIDs[i],
        		this->fluidEngine->GetFluidSet(this->fluidSetIDs[i]).GetParticleSize() / 1000,
        		"Gazebo/Red");
    }
}

//////////////////////////////////////////////////
void  FluidPlugin::InitObjectRendering()
{
    // get the vector of all IDs of object particle sets
    this->objectParticleSetIDs = this->fluidEngine->GetObjectParticleSetIDs();

    // for all object particle sets create the rendering entities
    for (int i = 0; i < this->fluidEngine->GetObjectSetCount(); ++i)
    {
        // initialise the size of the 2d vector with all particles positions
//        this->objectParticlesPositions2D.push_back(
//                std::vector<float3>(this->fluidEngine->GetParticleCount(
//                        this->objectParticleSetIDs[i])));

        this->objectParticlesPositions2D_x.push_back(
                std::vector<float>(this->fluidEngine->GetParticleCount(
                        this->objectParticleSetIDs[i])));
        this->objectParticlesPositions2D_y.push_back(
                std::vector<float>(this->fluidEngine->GetParticleCount(
                        this->objectParticleSetIDs[i])));
        this->objectParticlesPositions2D_z.push_back(
                std::vector<float>(this->fluidEngine->GetParticleCount(
                        this->objectParticleSetIDs[i])));

        // Create the rendering entities
        // has to be called in Init() (or later) after the SceneMgr is initialized
        // scale of 0.001 = 1m, one zero is for scaling down with 10 the whole fluidsim
        // TODO commented out for static objects
        this->CreateObjectRenderingEntities(
        		i, this->objectParticleSetIDs[i], 0.00004, "Gazebo/Blue");
    }
}

//////////////////////////////////////////////////
void  FluidPlugin::InitMovableObjectRendering()
{
    // get the vector of all IDs of object particle sets
    this->movableObjectParticleSetIDs = this->fluidEngine->GetMovableObjectParticleSetIDs();

    // for all object particle sets create the rendering entities
    for (int i = 0; i < this->fluidEngine->GetMovableObjectSetCount(); ++i)
    {
        // initialise the size of the 2d vector with all particles positions
//        this->movableObjectParticlesPositions2D.push_back(
//                std::vector<float3>(this->fluidEngine->GetParticleCount(
//                        this->movableObjectParticleSetIDs[i])));

        this->movableObjectParticlesPositions2D_x.push_back(
                std::vector<float>(this->fluidEngine->GetParticleCount(
                        this->movableObjectParticleSetIDs[i])));
        this->movableObjectParticlesPositions2D_y.push_back(
                std::vector<float>(this->fluidEngine->GetParticleCount(
                        this->movableObjectParticleSetIDs[i])));
        this->movableObjectParticlesPositions2D_z.push_back(
                std::vector<float>(this->fluidEngine->GetParticleCount(
                        this->movableObjectParticleSetIDs[i])));

        // Create the rendering entities
        // has to be called in Init() (or later) after the SceneMgr is initialized
        // scale of 0.001 = 1m, one zero is for scaling down with 10 the whole fluidsim
        this->CreateMovableObjectRenderingEntities(
        		i, this->movableObjectParticleSetIDs[i], 0.00004, "Gazebo/Green");
    }
}

//////////////////////////////////////////////////
void  FluidPlugin::CreateFluidRenderingEntities(
		int _index, int _setID, float _sphereSize, std::string _material)
{
    std::stringstream ss;

    // push back for each Set an Entity vector (the vector size will grow in the next loop)
    this->fluidEntities2D.push_back(std::vector<Ogre::Entity*>());

    // push back for each Set a SceneNode vector (the vector size will grow in the next loop)
    this->fluidSceneNodes2D.push_back(std::vector<Ogre::SceneNode*>());

    // loop through the number of particles of the current Set
    for (int j = 0; j < this->fluidEngine->GetParticleCount(_setID); j++)
    {
    	// create sphere entity name
        ss << "SphereEntity_s" << _setID << "p_" << j;

        // push back a created Entity, and set its Material
        this->fluidEntities2D[_index].push_back(this->mSceneMgr->createEntity(
                ss.str(), Ogre::SceneManager::PT_SPHERE));

        this->fluidEntities2D[_index][j]->setMaterialName(_material);

        // push back a SceneNode and attach the Entity to it
        this->fluidSceneNodes2D[_index].push_back(
                this->mSceneMgr->getRootSceneNode()->createChildSceneNode());

        this->fluidSceneNodes2D[_index][j]->attachObject(
        		this->fluidEntities2D[_index][j]);

        this->fluidSceneNodes2D[_index][j]->setScale(_sphereSize, _sphereSize, _sphereSize);

        this->fluidSceneNodes2D[_index][j]->setVisible(true);

        // clear the name of the entity
        ss.str("");
    }
}

//////////////////////////////////////////////////
void  FluidPlugin::CreateObjectRenderingEntities(
        int _index, int _setID, float _sphereSize, std::string _material)
{
    std::stringstream ss;

    // push back for each Set an Entity vector (the vector size will grow in the next loop)
    this->objectEntities2D.push_back(std::vector<Ogre::Entity*>());

    // push back for each Set a SceneNode vector (the vector size will grow in the next loop)
    this->objectSceneNodes2D.push_back(std::vector<Ogre::SceneNode*>());

    // loop through the number of particles of the current Set
    for (int j = 0; j < this->fluidEngine->GetParticleCount(_setID); j++)
    {
        // create sphere entity name
        ss << "SphereEntity_s" << _setID << "p_" << j;

        // push back a created Entity, and set its Material
        this->objectEntities2D[_index].push_back(this->mSceneMgr->createEntity(
                ss.str(), Ogre::SceneManager::PT_CUBE));

        this->objectEntities2D[_index][j]->setMaterialName(_material);

        // push back a SceneNode and attach the Entity to it
        this->objectSceneNodes2D[_index].push_back(
                this->mSceneMgr->getRootSceneNode()->createChildSceneNode());

        this->objectSceneNodes2D[_index][j]->attachObject(
                this->objectEntities2D[_index][j]);

        this->objectSceneNodes2D[_index][j]->setScale(_sphereSize, _sphereSize, _sphereSize);

        this->objectSceneNodes2D[_index][j]->setVisible(true);

        // clear the name of the entity
        ss.str("");
    }
}

//////////////////////////////////////////////////
void  FluidPlugin::CreateMovableObjectRenderingEntities(
        int _index, int _setID, float _sphereSize, std::string _material)
{
    std::stringstream ss;

    // push back for each Set an Entity vector (the vector size will grow in the next loop)
    this->movableObjectEntities2D.push_back(std::vector<Ogre::Entity*>());

    // push back for each Set a SceneNode vector (the vector size will grow in the next loop)
    this->movableObjectSceneNodes2D.push_back(std::vector<Ogre::SceneNode*>());

    // loop through the number of particles of the current Set
    for (int j = 0; j < this->fluidEngine->GetParticleCount(_setID); j++)
    {
        // create sphere entity name
        ss << "SphereEntity_s" << _setID << "p_" << j;

        // push back a created Entity, and set its Material
        this->movableObjectEntities2D[_index].push_back(this->mSceneMgr->createEntity(
                ss.str(), Ogre::SceneManager::PT_CUBE));

        this->movableObjectEntities2D[_index][j]->setMaterialName(_material);

        // push back a SceneNode and attach the Entity to it
        this->movableObjectSceneNodes2D[_index].push_back(
                this->mSceneMgr->getRootSceneNode()->createChildSceneNode());

        this->movableObjectSceneNodes2D[_index][j]->attachObject(
                this->movableObjectEntities2D[_index][j]);

        this->movableObjectSceneNodes2D[_index][j]->setScale(_sphereSize, _sphereSize, _sphereSize);

        this->movableObjectSceneNodes2D[_index][j]->setVisible(true);

        // clear the name of the entity
        ss.str("");
    }
}

//////////////////////////////////////////////////
void  FluidPlugin::RenderParticles()
{
    // set Fluid particles position
    for (int i = 0; i < this->fluidEngine->GetFluidSetCount(); ++i)
    {
    	// TODO check out where it's better to be updated (here or UpdateFluid)
//    	this->fluidEngine->GetParticlePositions(
//    			this->fluidSetIDs[i], this->fluidParticlesPositions2D[i]);

    	this->fluidEngine->GetParticlePositions(this->fluidSetIDs[i],
									this->fluidParticlesPositions2D_x[i],
									this->fluidParticlesPositions2D_y[i],
									this->fluidParticlesPositions2D_z[i]);

    	for (int j = 0; j < this->fluidEngine->GetParticleCount(
    			this->fluidSetIDs[i]); j++)
    	{

    	    if (isnan(this->fluidParticlesPositions2D_x[i][j]) ||
    	            isnan(this->fluidParticlesPositions2D_y[i][j]) ||
    	            isnan(this->fluidParticlesPositions2D_z[i][j]))
    	    {
    	        std::cout << " NaN received" << std::endl;
    	    }
    	    else
    	    {
        	this->fluidSceneNodes2D[i][j]->setPosition(
        			this->fluidParticlesPositions2D_x[i][j],
        			this->fluidParticlesPositions2D_y[i][j],
        			this->fluidParticlesPositions2D_z[i][j]);
    	    }
    	}
    }

    // set Object particles position
    for (int i = 0; i < this->fluidEngine->GetObjectSetCount(); ++i)
    {
        // TODO check out where it's better to be updated (here or UpdateFluid)
//        this->fluidEngine->GetParticlePositions(
//                this->objectParticleSetIDs[i], this->objectParticlesPositions2D[i]);

    	this->fluidEngine->GetParticlePositions(this->objectParticleSetIDs[i],
									this->objectParticlesPositions2D_x[i],
									this->objectParticlesPositions2D_y[i],
									this->objectParticlesPositions2D_z[i]);

        for (int j = 0; j < this->fluidEngine->GetParticleCount(
                this->objectParticleSetIDs[i]); j++)
        {
            this->objectSceneNodes2D[i][j]->setPosition(
                    this->objectParticlesPositions2D_x[i][j],
                    this->objectParticlesPositions2D_y[i][j],
                    this->objectParticlesPositions2D_z[i][j]);
        }
    }

    // set movable object particles position
    for (int i = 0; i < this->fluidEngine->GetMovableObjectSetCount(); ++i)
    {
        // TODO check out where it's better to be updated (here or UpdateFluid)
//        this->fluidEngine->GetParticlePositions(
//                this->movableObjectParticleSetIDs[i], this->movableObjectParticlesPositions2D[i]);

    	this->fluidEngine->GetParticlePositions(this->movableObjectParticleSetIDs[i],
									this->movableObjectParticlesPositions2D_x[i],
									this->movableObjectParticlesPositions2D_y[i],
									this->movableObjectParticlesPositions2D_z[i]);

        for (int j = 0; j < this->fluidEngine->GetParticleCount(
                this->movableObjectParticleSetIDs[i]); j++)
        {
            this->movableObjectSceneNodes2D[i][j]->setPosition(
                    this->movableObjectParticlesPositions2D_x[i][j],
                    this->movableObjectParticlesPositions2D_y[i][j],
                    this->movableObjectParticlesPositions2D_z[i][j]);
        }
    }
}

//////////////////////////////////////////////////
void FluidPlugin::InitWorldControlNode()
{
	// initialize Gazebo transport node
	this->node = transport::NodePtr(new transport::Node());

	// Initialize the node with the world name
	this->node->Init(this->worldName);

	// set advertising topic
	this->worldControlPub =
			this->node->Advertise<msgs::WorldControl>("~/world_control");
}

//////////////////////////////////////////////////
void FluidPlugin::PauseWorld()
{
    // Create and send a msg with pausing the world when the plugin is initialized
    msgs::WorldControl msg;
    msg.set_pause(true);
    this->worldControlPub->Publish(msg);
}

//////////////////////////////////////////////////
void FluidPlugin::StepWorld()
{
    // Create and send a msg with pausing the world when the plugin is initialized
    msgs::WorldControl msg;
    msg.set_step(true);
    this->worldControlPub->Publish(msg);
}

// Register this plugin with the simulator
GZ_REGISTER_SYSTEM_PLUGIN(FluidPlugin)

